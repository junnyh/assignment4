# include <iostream>
# include <vector>
# include <assert.h>
# include <R.h>

float updatePositionBlue(
    std::vector< std::vector<int> > &reds,
    std::vector< std::vector<int> > &blues,
    std::vector< std::vector<bool> > &occupied)
{
    const int blue = blues[0].size();
    const int row = occupied.size();
    std::vector<int> row_index(blues[0]);
    std::vector<int> col_index(blues[1]);
    std::vector<int> new_row_index(blue);
    float velocity = 0.0f;
    int moved_car = 0;
    
    for (int i = 0; i < blue; ++i)
    {
        if (row_index[i] == 0)
        {
            new_row_index[i] = row - 1;
        } 
        else 
        {
            new_row_index[i] = row_index[i] - 1;
        }

        if (!occupied[new_row_index[i]][col_index[i]])
        {
            ++ moved_car;
            occupied[new_row_index[i]][col_index[i]] = true;
            occupied[row_index[i]][col_index[i]] = false;
            blues[0][i] = new_row_index[i];
        }
    }
    velocity = (float) moved_car / (float) blue ;
    return velocity;
}


float updatePositionRed(
    std::vector< std::vector<int> > &reds,
    std::vector< std::vector<int> > &blues,
    std::vector< std::vector<bool> > &occupied)
{
    const int red = reds[0].size();
    const int col = occupied[0].size();
    std::vector<int> row_index(reds[0]);
    std::vector<int> col_index(reds[1]);
    std::vector<int> new_col_index(red);
    float velocity = 0.0f;
    int moved_car = 0;

    for (int i = 0; i < red; ++i)
    {
        if (col_index[i] == col - 1)
        {
            new_col_index[i] = 0;
        } 
        else 
        {
            new_col_index[i] = col_index[i] + 1;
        }

        if (!occupied[row_index[i]][new_col_index[i]])
        {
            ++moved_car;
            occupied[row_index[i]][new_col_index[i]] = true;
            occupied[row_index[i]][col_index[i]] = false;
            reds[1][i] = new_col_index[i];
        }
    }
    velocity = (float) moved_car / (float) red;
    return velocity;
}

float moveOnce(
    std::vector< std::vector<int> > &reds,
    std::vector< std::vector<int> > &blues,
    std::vector< std::vector<bool> > &occupied,
    unsigned int time)
{   
    float velocity;
    if (time % 2 == 1)
    {
        velocity = updatePositionBlue(reds, blues, occupied);
    }
    else
    {
        velocity = updatePositionRed(reds, blues, occupied);
    }
    return velocity;
}


void runBMLGrid(
    std::vector< std::vector<int> > &reds,
    std::vector< std::vector<int> > &blues,
    std::vector< std::vector<bool> > &occupied,
    unsigned int num_steps, float velocity = 0.0f)
{
    const int row  = occupied.size();
    const int col  = occupied[0].size();
    const int red  = reds[0].size();
    const int blue = blues[0].size();

    assert(row > 0 && col > 0);
    assert((red+blue) < (row*col));

    for (unsigned int t = 1; t < num_steps + 1; ++t)
    {
        std::vector< std::vector<int> > temp_r = reds;
        std::vector< std::vector<int> > temp_b = blues;
        std::vector< std::vector<bool> > temp_o = occupied;
        velocity = moveOnce(reds, blues, occupied, t);
    /*
        std::cout << "red:" << std::endl;
        for (std::vector<int> vec : reds)
        {
            for (int item : vec)
            {
                std::cout << item << " ";
            }
            std::cout << std::endl;
        }
        std::cout << "blue:" << std::endl;
        for (std::vector<int> vec : blues)
        {
            for (int item : vec)
            {
                std::cout << item << " ";
            }
            std::cout << std::endl;
        }
        std::cout << "occupied:" << std::endl;
        for (std::vector<bool> vec : occupied)
        {
            for (bool item : vec)
            {
                std::cout << item << " ";
            }
            std::cout << std::endl;
        }
        std::cout << "velocity:" << velocity << std::endl;
    */
    }
}

/*
int main()
{
    std::vector< std::vector<int> > reds = {
        {3,4,0,4},
        {1,2,3,3}
    };


    std::vector< std::vector<int> > blues = {
        {0,3,1,1},
        {0,0,1,2}
    };

    std::vector< std::vector<bool> > occupied {
        {1,0,0,1,0},
        {0,1,1,0,0},
        {0,0,0,0,0},
        {1,1,0,0,0},
        {0,0,1,1,0}
    };
    
    int num_steps = 5;

    runBMLGrid(reds, blues, occupied, num_steps);

    return 0;
}
*/
