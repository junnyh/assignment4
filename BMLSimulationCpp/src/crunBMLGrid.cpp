#include <Rcpp.h>
using namespace Rcpp;

// [[Rcpp::export(CppUpdatePositionBlue)]]
List CppUpdatePositionBlue(List g) {
  g["velocity"] = 0.0f;
  
	SEXP blues = g["blues"];
	SEXP occupied = g["occupied"];
  
 
	Rcpp::NumericMatrix cblues = as<Rcpp::NumericMatrix>(blues);
	Rcpp::LogicalMatrix coccupied = as<Rcpp::LogicalMatrix>(occupied);
	// Rcpp::NumericMatrix new_blues = as<Rcpp::NumericMatrix>(blues);
  int new_row;

	float velocity = 0.0f;
  int moved_car = 0;

  const int blue = cblues.nrow();
  const int row = coccupied.nrow();

  for (int i = 0; i < blue; ++i)
  {
  	if (cblues(i,0) == 1)
  	{
  		new_row = row;
  	}
  	else
  	{
  		new_row = cblues(i,0) - 1;
  	}
    
    if (!coccupied(new_row-1, cblues(i,1)-1)) 
  	{
  		++moved_car;
  		coccupied(new_row-1, cblues(i,1)-1) = true;
  		coccupied(cblues(i,0)-1, cblues(i,1)-1) = false;
  		cblues(i,0) = new_row;
  	}
  }
  velocity  = (float) moved_car / (float) blue;
  g["velocity"] = velocity;
  g["blues"] = cblues;
  g["occupied"] = coccupied;
  return g;
}

// [[Rcpp::export(CppUpdatePositionRed)]]
List CppUpdatePositionRed(List g) {
  g["velocity"] = 0.0f;
  
	SEXP reds = g["reds"];
	SEXP occupied = g["occupied"];

	Rcpp::NumericMatrix creds = as<Rcpp::NumericMatrix>(reds);
  
	Rcpp::LogicalMatrix coccupied = as<Rcpp::LogicalMatrix>(occupied);
	// Rcpp::NumericMatrix new_reds = as<Rcpp::NumericMatrix>(reds);
  
  int new_col;

	float velocity = 0.0f;
    int moved_car = 0;

    const int red = creds.nrow();
    const int col = coccupied.ncol();

    for (int i = 0; i < red; ++i)
    {
    	if (creds(i,1) == col)
    	{
    		new_col = 1;
    	}
    	else 
    	{
    		new_col = creds(i,1) + 1;
    	}

    	if (!coccupied(creds(i,0)-1, new_col-1)) 
    	{
    		++moved_car;
    		coccupied(creds(i,0)-1, new_col-1) = true;
    		coccupied(creds(i,0)-1, creds(i,1)-1) = false;
    		creds(i,1) = new_col;
    	}
    }
    velocity  = (float) moved_car / (float) red;
    g["velocity"] = velocity;
    g["reds"] = creds;
    g["occupied"] = coccupied;
    return g;
}

// [[Rcpp::export(CppMoveOnce)]]
List CppMoveOnce(List g, int t) {

  if (t % 2 == 1)
  {
		g = CppUpdatePositionBlue(g);
	}
	else
	{
		g = CppUpdatePositionRed(g);
	}

	return g;
}

// [[Rcpp::export(crunBMLGrid)]]
List crunBMLGrid (List g, int numSteps) {
  for (int t = 1; t < numSteps + 1; ++t) {
		g = CppMoveOnce(g, t);
	}
	return g;
}