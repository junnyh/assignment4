#' Assert Function
#' 
#' This Function return the error msg when expr is not satisfied
#' @param expr, error
#' @keywords check error
#' @return error message if expr is not satisfied
#' @export

assert <- function(expr, error){
  if (!expr) stop(error, call. = FALSE)
}